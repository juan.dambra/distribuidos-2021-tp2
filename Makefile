build: 
	docker-compose build

up:
	docker-compose up -d --remove-orphans

logs:
	docker-compose logs -f --tail=20

upl: up logs

stop:
	docker-compose stop

down:
	docker-compose down
	rm clientpy/debug.log -f

restart: down generate up logs 
	make down
	
generate:
	python docker-generator.py

punto1:
	d=$$(date +%s) \
	; make PUNTO=1 restart \
    && echo "Execution took $$(($$(date +%s)-d)) seconds"

punto2:
	d=$$(date +%s) \
	; make PUNTO=2 restart \
    && echo "Execution took $$(($$(date +%s)-d)) seconds"

punto3:
	d=$$(date +%s) \
	; make PUNTO=3 restart \
    && echo "Execution took $$(($$(date +%s)-d)) seconds"

punto0:
	d=$$(date +%s) \
	; make PUNTO=0 restart \
    && echo "Execution took $$(($$(date +%s)-d)) seconds"
