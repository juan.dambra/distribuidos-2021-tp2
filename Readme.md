### Correr en local. 

El sistema depende de la instalacion de pipenv, un programa para manejar ambientes de python y dependencias. En caso de que no se instale se necesitan las dependencias declaradas solamente en el archivo docker-generator.py, que se encarga de crear los docker-compose.yml

### Configuraciones de tp2-config.yml

* LOGLEVEL: INFO. Acepta las variables de DEBUG,INFO,WARNING,ERROR. Define el nivel de log que escupe la aplicacion
* DEBUG=FALSE Si el valor vale True, empieza a loguear por consola con el nivel
* Por cada punto existen variables globales, como   
  * CHUNK_SIZE: 2000 - Define el tamaño de chunk que el reader ingresa al sistema
  * TOTAL_LENGTH: 10000 - Define el tamaño total del archivo que se lee. Para leer todo el archivo debe usarse el valor -1
  
Por cada worker existen las siguientes variables que pueden o no existir.
* STEP: 4 Define el step, haciendo que cree una queue con ese numero. (OBLIGATORIO) 
* WORKERS: 1 Define la cantidad de workers por Etapa. Depende del proceso puede crecer en cantidad de workers o no. 
* INTERMEDIATE: false - Por default esta en true. En caso de estar en false, entonces se ejecuta un end_job al finalizar el trabajo en la etapa posterior, y se transmite a la siguiente etapa 
* RUNNER: punto1/score_adder - Script desde donde ejecuta el paso (OBLIGATORIO) 
* SERVICE_NAME: reducer - Nombre del servicio (OBLIGATORIO e UNICO por punto)

Para los casos donde se quiere usar afinidad, se usan en conjunto un chunk_splitter con los parametros
* AFFINITY_AMOUNT: 7, donde especifica cuantos distintos indices de afinidad se van a enviar mensajes. Esto deberia venir apareado con un valor en la proxima etapa, con AFFINITY: true, que bindea una cola por ID, y la cantidad de workers de la etapa siguiente deberia ser la misma que el parametro AFFINITY_AMOUNT de la etapa anterior.
