import signal
import pika, os
from utils.utils import (
    WORKER_START_ACK,
    WORKER_START_END,
    WORKER_END,
    MESSAGE_TRIGGER,
    WORKER_START,
    STATUS_ON,
    STATUS_OFF,
    Message,
    decode_string,
)
from operator import itemgetter
import time

from src.utils.logger import logging

logger = logging.getLogger("Coordinator")

amount_steps = int(os.environ.get("AMOUNT_STEPS", 4))

logger.info("Starting connection")
rabbitmq_host = os.environ.get("RABBITMQ_HOST", "rabbitmq")

connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitmq_host))
channel = connection.channel()


def exit_gracefully(self, *args):
    logger.info("Caught signal, exiting")
    channel.stop_consuming()
    connection.close()
    os._exit(0)


signal.signal(signal.SIGINT, exit_gracefully)
signal.signal(signal.SIGTERM, exit_gracefully)


channel.queue_declare(queue="control")
channel.exchange_declare(exchange="control-coordinator", exchange_type="topic")
channel.exchange_declare(exchange="control-coordinator-global", exchange_type="fanout")

logger.info("Connected")

clients = []
steps = {}

last_client_time = time.time()


def find_client(clients, client_msg):
    id, step, service_name = itemgetter("id", "step", "service_name")(client_msg)
    client_search = [
        client
        for client in clients
        if client["id"] == id
        and client["step"] == step
        and client["service_name"] == service_name
    ]
    return client_search


def add_client(clients, steps, msg):
    id, step, service_name, next_step = itemgetter(
        "id", "step", "service_name", "next_step"
    )(msg)
    client_search = find_client(clients, msg)
    if len(client_search) == 0:
        clients.append(
            {
                "id": id,
                "step": step,
                "service_name": service_name,
                "status": STATUS_ON,
                "next_step": str(next_step),
            }
        )
    else:
        client_search["status"] = STATUS_ON
    if step in steps:
        if steps[step]["status"] == STATUS_OFF:
            logger.warning(
                f"STEP OFF for Client {id}, for step {step}, service name {service_name}"
            )
            return False
    logger.debug(f"Adding Client {id}, step {step}, service name {service_name}")
    logger.debug("Clients %s", clients)
    return True


def end_client(clients, msg):
    id, step, service_name, next_step = itemgetter(
        "id", "step", "service_name", "next_step"
    )(msg)
    client_search = find_client(clients, msg)
    if len(client_search) == 0:
        logger.debug(f"No Client {msg} yet")
        clients.append(
            {
                "id": id,
                "step": step,
                "service_name": service_name,
                "status": STATUS_ON,
                "next_step": str(next_step),
            }
        )
    else:
        client_search[0]["status"] = STATUS_OFF
        return True


def all_clients_ended_step(clients, step, next_step):
    # id, step, service_name = itemgetter("id", "step", "service_name")(client_msg)
    client_status = [
        client["status"] for client in clients if client["next_step"] == next_step
    ]
    clients_on = [status for status in client_status if status == STATUS_ON]
    if len(clients_on) > 0:
        return False
    else:
        return True


def all_clients_ended(clients: dict):
    client_status = [client["status"] for client in clients]
    clients_on = [status for status in client_status if status == STATUS_ON]
    if len(clients_on) > 0:
        return False
    else:
        return True


def all_steps_started(clients: dict, amount_steps):
    client_status = [
        client["service_name"] for client in clients if client["status"] == STATUS_ON
    ]
    logger.debug("Clients %s %s", set(client_status), len(set(client_status)))
    if len(set(client_status)) == amount_steps:
        return True
    else:
        return False


def trigger_end(channel, steps: dict, step: str):
    logger.debug(f"COORDINATOR:: Triggering End for step {step}")

    if step in steps:
        msg = Message({"type": WORKER_START_END})

        channel.basic_publish(
            exchange="control-coordinator",
            routing_key=f"step.{step}",
            body=msg.to_string(),
        )

        steps[step]["status"] = STATUS_OFF
    else:
        logger.debug(steps)
        logger.debug(f"No step registered for step {step}")


def trigger_start(channel, client, steps):
    logger.info(f"Triggering START")
    msg = Message({"type": WORKER_START_ACK})
    channel.basic_publish(
        exchange="control-coordinator-global", routing_key="", body=msg.to_string()
    )
    for client in clients:
        steps[client["step"]] = {"status": STATUS_ON}
    logger.debug(f"Triggered Start")


def end_client_steps(clients, steps, msg):
    if end_client(clients, msg):
        if all_clients_ended_step(clients, msg["step"], str(msg["next_step"])):
            trigger_end(channel, steps, str(msg["next_step"]))


def message_control(channel, method_frame, header_frame, body):
    msg = decode_string(body)
    logger.debug(f" [*] Recieved: {msg}")
    if msg["type"] == MESSAGE_TRIGGER:
        trigger_end(channel, steps, msg["step"])
    if msg["type"] == WORKER_START:
        if add_client(clients, steps, msg):
            if all_steps_started(clients, amount_steps):
                trigger_start(channel, clients, steps)
        else:
            end_client_steps(clients, steps, msg)
    if msg["type"] == WORKER_END:
        end_client_steps(clients, steps, msg)
        if all_clients_ended(clients):
            channel.close()


channel.basic_consume("control", message_control, auto_ack=True)

try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()
connection.close()
