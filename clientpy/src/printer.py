#!/usr/bin/env python
from workers.worker import Worker
from processors import ChunkProcessor

from src.utils.logger import logging

logger = logging.getLogger("Printer")


def end_func(state):
    logger.info(f"Final Result: {state}")


def map_func(msg, state):
    state["Result"] = msg
    # logger.info(f"Result: {state}")
    return msg


worker = Worker(ChunkProcessor(map_func, end_job=end_func))
worker.start()
worker.join()
