#!/usr/bin/env python
from functools import reduce
import src.utils.utils as utils
from src.utils.logger import logging
import os

logger = logging.getLogger("Base Processor")


class BaseProcessor:
    @staticmethod
    def print_state(state):
        logger.debug("End Job. State: %s", state)
        return False

    @staticmethod
    def default_affinity_func(msg,affinity, state):
        if msg["id"] % affinity == 0:
            return True
        return False

    @staticmethod
    def default_process_func(msg, state):
        state["process"] = msg
        return msg

    @staticmethod
    def default_filter_func(msg):
        return True

    @staticmethod
    def default_map_func(msg, state):
        return msg

    @staticmethod
    def default_reduce_func(acc, val):
        return acc + val["Score"]

    def __init__(self, end_job=None, affinity_func=None):
        self.init_val = {}
        self.end_job = end_job if end_job is not None else self.print_state
        self.intermediate = os.environ.get("INTERMEDIATE", "True").lower() in (
            "true",
            "1",
            "t",
        )

    def get_init_val(self):
        return self.init_val

    def generate_callback(self, next_queue, state):
        def callback_step_inner(channel, method_frame, header_frame, body):
            chunk = utils.decode_string(body)
            reduce(self.reduce_func, chunk, state["processor"])

        return callback_step_inner
