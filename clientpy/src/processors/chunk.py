#!/usr/bin/env python
from src.processors.base_processor import BaseProcessor
from src.utils.utils import decode_string, encode_to_string
import os
from src.utils.logger import logging

logger = logging.getLogger("ChunkProcessor")


class ChunkProcessor(BaseProcessor):
    def __init__(self, process_func=None, end_job=None, affinity_func=None):
        super().__init__(end_job)
        self.process_func = (
            process_func if process_func is not None else self.default_process_func
        )
        self.affinity_func = (
            affinity_func if affinity_func is not None else self.default_affinity_func
        )
        self.affinity_amount = int(os.environ.get("AFFINITY_AMOUNT", "0"))

    def generate_callback(self, next_queue, state=None):
        def callback_step_inner(channel, method_frame, header_frame, body):
            chunk = decode_string(body)
            res = self.process_func(chunk, state)
            if self.affinity_amount >= 1:
                logger.debug("Is affinity")
                arr_key = "id"
                if type(res) == dict:
                    arr_res = list(res.values())[0]
                    arr_key = list(res.keys())[0]
                    logger.debug(f"Is Dict with key {arr_key}")

                else:
                    arr_res = res
                for aff in range(self.affinity_amount):
                    aff_result = [
                        r
                        for r in arr_res
                        if self.affinity_func(r, self.affinity_amount, aff, state)
                        == True
                    ]
                    # logger.debug("Affinity Result %s for key %s", aff_result, aff)
                    logger.debug(
                        "Difference in len for aff %s: original %s, affinity %s",
                        aff,
                        len(res),
                        len(aff_result),
                    )

                    if len(aff_result) > 0:
                        if type(res) == dict:
                            aff_result = {arr_key: aff_result}
                        channel.basic_publish(
                            exchange="jobs",
                            routing_key=f"{next_queue}.{aff}",
                            body=encode_to_string(aff_result),
                        )
                pass
            else:
                if self.intermediate:
                    channel.basic_publish(
                        exchange="jobs",
                        routing_key=next_queue,
                        body=encode_to_string(res),
                    )
                else:
                    state["processor"] = res

        return callback_step_inner
