#!/usr/bin/env python
from src.utils.utils import decode_string, encode_to_string
from .base_processor import BaseProcessor


from src.utils.logger import logging

logger = logging.getLogger("Processor")


class Processor(BaseProcessor):
    def __init__(
        self,
        map_func=None,
        filter_func=None,
        end_job=None,
        affinity_func=None,
        affinity_amount=1,
    ):
        super().__init__(end_job)
        self.map_func = map_func if map_func is not None else self.default_map_func
        self.filter_func = (
            filter_func if filter_func is not None else self.default_filter_func
        )
        self.affinity_func = affinity_func
        self.affinity_amount = affinity_amount

    def generate_callback(self, next_queue, state=None):
        def callback_step_inner(channel, method_frame, header_frame, body):
            chunk = decode_string(body)
            if len(chunk) > 0:
                chunk_new = [
                    self.map_func(msg, state)
                    for msg in chunk
                    if self.filter_func(msg) == True
                ]
                state["processor"] = chunk_new
                if len(chunk_new) > 0:
                    if self.affinity_func is None:
                        # if not self.end_job is None:
                        chunk_new_end = self.end_job(state)
                        if chunk_new_end != False:
                            chunk_new = chunk_new_end
                        logger.debug(f"Sending Chunk {chunk_new} to queue {next_queue}")
                        channel.basic_publish(
                            exchange="jobs",
                            routing_key=next_queue,
                            body=encode_to_string(chunk_new),
                        )
                    else:
                        for i in range(self.affinity_amount):
                            chunk_afin = [
                                msg
                                for msg in chunk
                                if self.affinity_func(msg, i) == True
                            ]
                            if len(chunk_afin) > 0:
                                channel.basic_publish(
                                    exchange="jobs",
                                    routing_key=f"{next_queue}.{i}",
                                    body=encode_to_string(chunk_afin),
                                )

        return callback_step_inner
