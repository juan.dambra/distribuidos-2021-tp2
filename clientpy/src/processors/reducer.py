#!/usr/bin/env python
from functools import reduce
import copy
from src.processors.base_processor import BaseProcessor
from src.utils.utils import encode_to_string, decode_string


from src.utils.logger import logging

logger = logging.getLogger("Reducer")


class Reducer(BaseProcessor):
    def __init__(self, reduce_func=None, init_val={}, end_job=None):
        super().__init__(end_job)
        self.reduce_func = (
            reduce_func if reduce_func is not None else self.default_reduce_func
        )
        self.init_val = init_val
        self.end_job = end_job if end_job is not None else self.print_state

    def generate_callback(self, next_queue, state):
        def callback_step_inner(channel, method_frame, header_frame, body):
            chunk = decode_string(body)
            if type(chunk) == dict:
                chunk = chunk.values()
            if self.intermediate:
                logger.debug("Reducer IS INTERMEDIATE",)
                cloned = copy.deepcopy(self.init_val)
                res = reduce(self.reduce_func, chunk, self.init_val)
                self.init_val = cloned
                logger.debug(f"Sending {res} to {next_queue}",)
                channel.basic_publish(
                    exchange="jobs",
                    routing_key=next_queue,
                    body=encode_to_string(res),
                )
            else:
                logger.debug("Not Intermediate, processing chunk %s",chunk)
                res = reduce(self.reduce_func, chunk, state["processor"])
                # logger.debug("Processed reducer on %s", res)
                state["processor"] = res

        return callback_step_inner
