from src.workers import WorkerProducer

worker = WorkerProducer("answers.csv")
worker.start()
worker.join()
