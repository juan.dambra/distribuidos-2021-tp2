#!/usr/bin/env python
from time import sleep
from src.workers import Worker
from src.processors import Reducer

from src.utils.logger import logging

logger = logging.getLogger("Reducer")


column_adder = "sentiment"


def reducer_func(acc, val):
    acc[column_adder] += int(val[column_adder])
    acc["count"] += 1
    return acc


def print_state(state):
    if "processor" in state:
        result = state["processor"][column_adder] / state["processor"]["count"]
        logger.debug("END JOB:")
        logger.debug("State: %s", state)
        return {"Negative Sentiment Ratio": result}
    else:
        return {"Result": "No result"}


worker = Worker(Reducer(reducer_func, {column_adder: 0, "count": 0}, print_state, ))
worker.start()
worker.join()
