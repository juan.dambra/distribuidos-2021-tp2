#!/usr/bin/env python
from time import sleep
from src.workers import Worker
from src.processors import Processor
from nltk.sentiment.vader import SentimentIntensityAnalyzer


def map_func(msg,state):
    body = msg.pop("Body")
    sentiment_analyzer = SentimentIntensityAnalyzer()
    sentiment = sentiment_analyzer.polarity_scores(body)["compound"]
    if sentiment < 0:
        msg["sentiment"] = 1
    else:
        msg["sentiment"] = 0
    return msg


worker = Worker(Processor(map_func))
worker.start()
worker.join()
