#!/usr/bin/env python
from time import sleep
from src.workers import Worker
from src.processors import Processor
from nltk.sentiment.vader import SentimentIntensityAnalyzer


def map_func(msg):
    return msg


worker = Worker(Processor(map_func))
worker.start()
worker.join()
