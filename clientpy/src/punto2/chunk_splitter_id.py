#!/usr/bin/env python
from src.workers.worker import Worker
from src.processors import ChunkProcessor

from src.utils.logger import logging
import os

logger = logging.getLogger("Printer")
key = "OwnerUserId"


def affinity_func(msg, aff, val, state):
    id = int(msg[key].strip(".0"))
    # logger.debug("aff comparison: %s - %s", id, aff)
    if id % aff == val:
        return True
    return False


worker = Worker(
    ChunkProcessor(
        affinity_func=affinity_func,
    )
)
worker.start()
worker.join()
