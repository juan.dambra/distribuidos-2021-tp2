#!/usr/bin/env python
from time import sleep
from src.workers import Worker
from src.processors import Processor


def map_func(msg, state):
    new_msg = {}
    columns = ["Score"]
    for column in columns:
        new_msg[column] = msg[column]
    # print("Droping Columns", msg, new_msg)
    return new_msg


worker = Worker(Processor(map_func))
worker.start()
worker.join()
