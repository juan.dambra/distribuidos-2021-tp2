#!/usr/bin/env python
import logging
from time import sleep
from src.workers import Worker
from src.processors import Processor


from src.utils.logger import logging

logger = logging.getLogger("Column droper")


def map_func(msg, state):
    new_msg = {}
    columns = ["Score", "OwnerUserId"]
    for column in columns:
        new_msg[column] = msg[column]
    # logger.debug("New MSG %s", new_msg)
    return new_msg


def filter_func(msg):
    field = "OwnerUserId"
    return True if msg[field] != "" else False


worker = Worker(Processor(map_func, filter_func))
worker.start()
worker.join()
