#!/usr/bin/env python
from src.workers.worker import Worker
from src.processors import ChunkProcessor

from src.utils.logger import logging

logger = logging.getLogger("Printer")


def end_func(state):
    logger.debug(f"Final Result: {state}")
    ids_greater = []
    scores = state["avg_group"]["Score"]
    for id in scores:
        msg = scores[id]
        if (msg["val"] / msg["count"]) > state["avg_general"]:
            ids_greater.append({"id": id, "score": msg["val"]})

    logger.debug(f"Ids Greater: {ids_greater}")
    return {"answers_ids_greater": ids_greater}


def map_func(msg, state):
    for key in msg:
        state[key] = msg[key]
    return msg


worker = Worker(ChunkProcessor(map_func, end_job=end_func))
worker.start()
worker.join()
