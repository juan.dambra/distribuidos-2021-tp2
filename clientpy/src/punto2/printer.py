#!/usr/bin/env python
from src.workers.worker import Worker
from src.processors import ChunkProcessor

from src.utils.logger import logging

logger = logging.getLogger("Printer")


def end_func(state):
    logger.debug(f"Final Result: {state}")
    questions = state["processor"]["questions_ids_greater"]
    answers = state["processor"]["answers_ids_greater"]
    logger.debug("Getting unique ids")
    l1 = [al["id"] for al in questions]
    l2 = [al["id"] for al in answers]
    ids = list(set(l1) & set(l2))
    ranking = {}
    logger.debug("Merging ids of unique length %s", len(ids))

    for question in questions:
        id = question["id"]
        if id in ids:
            ranking[id] = {"id": id, "score": question["score"]}

    for answer in answers:
        id = answer["id"]
        if id in ids:
            if id in ranking:
                ranking[id]["score"] += answer["score"]
            else:
                ranking[id] = {"id": id, "score": answer["score"]}

    return {"merge": ranking}
    # for id in ids:
    #     [question for question in questions if question["id"] == id][0]
    #     answer = [answer for answer in answers if answer["id"] == id][0]
    # #     ranking.append({"id": id, "score": question["score"] + answer["score"]})
    # ranking = list(ranking.values())
    # logger.debug("Merged ids")
    # # logger.info("Merged ids %s",ranking)

    # ranking.sort(key=lambda x: x.get("score"), reverse=True)
    # logger.info("Result, %s", ranking[:10])


def map_func(msg, state):
    for key in msg:
        logger.debug("key, %s", key)
        state["processor"][key] = msg[key]

    return state["processor"]


worker = Worker(ChunkProcessor(map_func, end_job=end_func))
worker.start()
worker.join()
