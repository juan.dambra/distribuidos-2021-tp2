from src.workers import WorkerProducer

worker = WorkerProducer("questions.csv")
worker.start()
worker.join()
