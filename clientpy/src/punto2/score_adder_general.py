#!/usr/bin/env python
from src.workers import Worker
from src.processors import Reducer

from src.utils.logger import logging

logger = logging.getLogger("Score adder General")
column_adder = "Score"


def reducer_func(acc, val):
    logger.debug(f"{acc},{val}")
    acc[column_adder]["val"] += val["val"]
    acc[column_adder]["count"] += val["count"]
    return acc


def end_state(state):
    if "processor" in state:
        logger.debug("End State to process: %s", state["processor"])
        proc_state = state["processor"][column_adder]
        return {"avg_general": proc_state["val"] / proc_state["count"]}
        # result = state["processor"][column_adder] / state["processor"]["count"]
        # logger.debug("END JOB:")
        # return {"Result": state["processor"]}
    else:
        return {"Result": "No result"}


worker = Worker(
    Reducer(reducer_func, {column_adder: {"val": 0, "count": 0}}, end_state)
)
worker.start()
worker.join()
