#!/usr/bin/env python
from src.workers import Worker
from src.processors import Reducer

from src.utils.logger import logging

logger = logging.getLogger("Reducer")
column_adder = "Score"


def reducer_func(acc, val):
    # logger.debug(f"{acc},{val}")
    acc[column_adder]["val"] += int(val[column_adder])
    acc[column_adder]["count"] += 1
    return acc


def print_state(state):
    if "processor" in state:
        logger.debug("End State to send: %s", state["processor"])
        # result = state["processor"][column_adder] / state["processor"]["count"]
        # logger.debug("END JOB:")
        return {"Result": state["processor"]}
    else:
        return {"Result": "No result"}


worker = Worker(
    Reducer(reducer_func, {column_adder: {"val": 0, "count": 0}}, print_state)
)
worker.start()
worker.join()
