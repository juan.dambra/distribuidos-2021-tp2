#!/usr/bin/env python
from time import sleep
from src.workers import Worker
from src.processors import Reducer

from src.utils.logger import logging

logger = logging.getLogger("Reducer")


column_adder = "Score"
id = "OwnerUserId"


def reducer_func(acc, val):
    # logger.debug(f"{acc},{val}")
    local_id = val[id]
    col_acc = acc[column_adder]
    if not local_id in col_acc:
        col_acc[local_id] = {"val": 0, "count": 0}
    col_acc[local_id]["val"] += int(val[column_adder])
    col_acc[local_id]["count"] += 1
    return acc


def print_state(state):
    if "processor" in state:
        logger.debug("State: %s", state)
        # result = state["processor"][column_adder] / state["processor"]["count"]
        # logger.debug("END JOB:")
        return {"avg_group": state["processor"]}
    else:
        return {"Result": "No result"}


worker = Worker(Reducer(reducer_func, {column_adder: {}}, print_state))
worker.start()
worker.join()
