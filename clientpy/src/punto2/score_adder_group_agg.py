#!/usr/bin/env python
from time import sleep
from src.workers import Worker
from src.processors import Reducer

from src.utils.logger import logging

logger = logging.getLogger("Reducer")


column_adder = "Score"
id = "OwnerUserId"


def reducer_func(acc, val):
    logger.debug(f"{len(acc[column_adder])},{len(val)}")
    for key in val:
        if key in acc[column_adder]:
            vals = acc[column_adder][key]
            vals["count"] += val[key]["count"]
            vals["val"] += val[key]["val"]
        else:
            acc[column_adder][key] = val[key]
    logger.debug(f"{len(acc[column_adder])}")

    return acc


def print_state(state):
    if "processor" in state:
        # logger.debug("State: %s", state)
        # result = state["processor"][column_adder] / state["processor"]["count"]
        # logger.debug("END JOB:")
        return {"avg_group": state["processor"]}
    else:
        return {"Result": "No result"}


worker = Worker(Reducer(reducer_func, {column_adder: {}}, print_state))
worker.start()
worker.join()
