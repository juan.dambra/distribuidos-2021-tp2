#!/usr/bin/env python
from src.workers.worker import Worker
from src.processors import ChunkProcessor

from src.utils.logger import logging

logger = logging.getLogger("Printer")


def end_func(state):
    logger.debug(f"Final Result: {state}")
    ranking = state["processor"]
   
    # for id in ids:
    #     [question for question in questions if question["id"] == id][0]
    #     answer = [answer for answer in answers if answer["id"] == id][0]
    #     ranking.append({"id": id, "score": question["score"] + answer["score"]})
    ranking = list(ranking.values())
    logger.debug("Merged ids")
    # logger.info("Merged ids %s",ranking)

    ranking.sort(key=lambda x: x.get("score"), reverse=True)
    logger.info("Result, %s", ranking[:10])


def map_func(msg, state):
    for key in msg["merge"]:
        logger.debug("key, %s", key)
        state["processor"][key] = msg["merge"][key]
    return state["processor"]


worker = Worker(ChunkProcessor(map_func, end_job=end_func))
worker.start()
worker.join()
