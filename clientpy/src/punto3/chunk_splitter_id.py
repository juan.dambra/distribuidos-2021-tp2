#!/usr/bin/env python
from src.workers.worker import Worker
from src.processors import ChunkProcessor

from src.utils.logger import logging
import os

logger = logging.getLogger("Printer")
key1 = "Id"
key2 = "ParentId"


def affinity_func(msg, aff, val, state):
    # logger.debug("aff comparison: %s - %s %s", aff, val, msg)
    if key2 in msg:
        id = int(msg[key2].strip(".0"))
    else:
        id = int(msg[key1].strip(".0"))
    if id % aff == val:
        return True
    return False


# def process_func(chunk, state):
#     for key in chunk:
#         chunk[key]

worker = Worker(
    ChunkProcessor(
        # process_func=process_func,
        affinity_func=affinity_func,
    )
)
worker.start()
worker.join()
