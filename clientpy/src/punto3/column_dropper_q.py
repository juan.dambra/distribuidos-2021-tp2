#!/usr/bin/env python
import logging
from src.workers import Worker
from src.processors import Processor


from src.utils.logger import logging

logger = logging.getLogger("Column droper")


def map_func(msg, state):
    new_msg = {}
    columns = ["Id", "CreationDate", "Score", "Tags"]
    # logger.debug("Old MSG, %s", msg)

    for column in columns:
        new_msg[column] = msg[column]
    # logger.debug("New MSG %s", new_msg)
    year = new_msg["CreationDate"].split("-")[0]
    new_msg["CreationDate"] = year
    return new_msg


def end_job(state):
    # logger.debug("Current state, %s", state)
    return {"q_drop": state["processor"]}


worker = Worker(Processor(map_func, None, end_job=end_job))
worker.start()
worker.join()
