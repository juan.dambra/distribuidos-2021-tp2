#!/usr/bin/env python
import logging
from src.workers import Worker
from src.processors import Processor


from src.utils.logger import logging

logger = logging.getLogger("Column droper")


def map_func(msg, state):
    new_msg = {}
    columns = ['CreationDate', 'Score', 'Tags']
    # logger.debug("Old MSG, %s", msg)
    msg["Score"] = int(msg["r_Score"]) + int(msg["Score"])
    for column in columns:
        new_msg[column] = msg[column]
    return new_msg


# def end_job(state):
    # logger.debug("Current state, %s", state)
    # return {"q_drop": state["processor"]}


worker = Worker(Processor(map_func, None, end_job=None))
worker.start()
worker.join()
