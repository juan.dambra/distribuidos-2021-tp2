#!/usr/bin/env python
from src.workers.worker import Worker
from src.processors import ChunkProcessor
from src.utils.logger import logging

logger = logging.getLogger("Printer")

left_key = "Id"
right_key = "ParentId"
left_data = "q_drop"
right_data = "a_drop"


def build_index(data, key):
    return {item[key]: item for item in data}


def find_in_data(data, key_name, key):
    return [d for d in data if d[key_name] == key]


def find_in_data_idx(data, key):
    return data.get(key, None)


def end_func(state):
    logger.debug(f"State: {state.keys()}")
    left = state["processor"][left_data]
    right = state["processor"][right_data]
    logger.debug(f"right len: {len(right)}, left len: {len(left)}")
    index = build_index(left, left_key)

    merged = []
    for right_e in right:
        # logger.debug(f"right_e: {right_e}")
        key = right_e[right_key]
        left_e = find_in_data_idx(index, key)
        # logger.debug(f"left_e: {left_e}")
        if not left_e is None:
            # left_e = left_e[0]
            final = {**left_e}
            for key in right_e:
                final[f"r_{key}"] = right_e[key]
            # logger.debug(f"final: {final}")
            merged.append(final)
    logger.debug(f"Final Result: len => {len(merged)}, {merged}")
    return {"merge": merged}


def process_func(msg, state):
    for key in msg:
        logger.debug("key, %s", key)
        if key in state["processor"]:
            state["processor"][key] += msg[key]
        else:
            state["processor"][key] = msg[key]
    # logger.debug("State, %s", state)
    return state["processor"]


worker = Worker(ChunkProcessor(process_func, end_job=end_func))
worker.start()
worker.join()
