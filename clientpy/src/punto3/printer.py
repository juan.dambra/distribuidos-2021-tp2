#!/usr/bin/env python
from src.workers.worker import Worker
from src.processors import ChunkProcessor

from src.utils.logger import logging

logger = logging.getLogger("Printer")


def end_func(state):
    logger.debug(f"Final Result: {state}")



def map_func(msg, state):
    for key in msg:
        if key in state:
            state[key] += msg[key]
        else:
            state[key] = msg[key]
    logger.debug("State, %s", state)
    return msg


worker = Worker(ChunkProcessor(map_func, end_job=end_func))
worker.start()
worker.join()
