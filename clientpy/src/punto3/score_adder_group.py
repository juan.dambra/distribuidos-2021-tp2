#!/usr/bin/env python
from time import sleep
from src.workers import Worker
from src.processors import Reducer

from src.utils.logger import logging

logger = logging.getLogger("Reducer")


column_adder = "Score"
id = "CreationDate"


def reducer_func(acc, val):
    # logger.debug(f"{acc},{val}")
    local_id = val.pop(id)
    col_acc = acc[column_adder]
    if not local_id in col_acc:
        col_acc[local_id] = []
    col_acc[local_id].append(val)
    return acc


def print_state(state):
    if "processor" in state:
        logger.debug("State: %s", state)
        # result = state["processor"][column_adder] / state["processor"]["count"]
        # logger.debug("END JOB:")
        return {"group_year": state["processor"]}
    else:
        return {"Result": "No result"}


worker = Worker(Reducer(reducer_func, {column_adder: {}}, print_state))
worker.start()
worker.join()
