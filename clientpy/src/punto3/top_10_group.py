#!/usr/bin/env python
from src.workers.worker import Worker
from src.processors import ChunkProcessor
from src.utils.logger import logging

logger = logging.getLogger("Top10")


def end_func(state):
    logger.debug(f"Final Result: {state}")

    state_group = state["processor"]["Score"]
    scorer = {}
    for year in state_group:
        scorer_tag = []
        score_year = state_group[year]
        logger.debug(f"Year {year}")
        unique_tags = set([el["Tags"] for el in score_year])
        logger.debug(f"Unique Tags len {len(unique_tags)}")

        tag_idx = {}
        for el in score_year:
            tag = el["Tags"]
            val = el["Score"]
            if tag in tag_idx:
                tag_idx[tag]["Score"] += val
            else:
                tag_idx[tag] = {"Tag": tag, "Score": val}  

        scorer_tag = list(tag_idx.values())
        logger.debug(f"Scorer Tags len {len(scorer_tag)}")
        scorer_tag.sort(key=lambda x: x.get("Score"), reverse=True)
        scorer[year] = scorer_tag[:10]
        logger.debug("Year %s => %s", year, scorer_tag[:10])

    logger.info("Scorer %s", scorer)


subkey = "group_year"


def map_func(msg, state):
    for key in msg[subkey]["Score"]:
        logger.debug("key, %s", key)
        if not "Score" in state["processor"]:
            state["processor"]["Score"] = {}
        state["processor"]["Score"][key] = msg[subkey]["Score"][key]
    return state["processor"]


worker = Worker(ChunkProcessor(map_func, end_job=end_func))
worker.start()
worker.join()
