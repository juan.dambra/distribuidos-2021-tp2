#!/usr/bin/env python
from src.workers.worker import Worker
from src.processors import ChunkProcessor

from src.utils.logger import logging

logger = logging.getLogger("Unwinder")

unwind_key = "Tags"
unwind_val = " "
chunk_key = "merge"

def end_func(state):
    # left = state[left_data]
    # right = state[right_data]
    # merged = []
    # for right_e in right:
    #     # logger.debug(f"right_e: {right_e}")
    #     key = right_e[right_key]
    #     left_e = find_in_data(left, left_key, key)
    #     # logger.debug(f"left_e: {left_e}")
    #     if len(left_e) > 0:
    #         left_e = left_e[0]
    #         final = {**left_e}
    #         for key in right_e:
    #             final[f"r_{key}"] = right_e[key]
    #         logger.debug(f"final: {final}")
    #         merged.append(final)
    logger.debug(f"Final Result: {state}")
    return {"merge": state}


def generate_msg(msg, val):
    new_msg = {**msg}
    new_msg[unwind_key] = val
    return new_msg


def process_func(chunk, state):
    logger.debug(f"Processing Chunk: {chunk}")

    msgs = []
    for msg in chunk[chunk_key]:
        unwinded = [generate_msg(msg, tag) for tag in msg[unwind_key].split(unwind_val)]
        msgs += unwinded
        logger.debug(f"New unwinded: {unwinded}")
    return msgs


worker = Worker(ChunkProcessor(process_func, end_job=None))
worker.start()
worker.join()
