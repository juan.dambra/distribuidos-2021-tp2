#!/usr/bin/env python
import csv
from operator import length_hint
from src.utils.logger import logging
import math

logger = logging.getLogger("Reader")

row_names = {
    "answers.csv": [
        "Row",
        "Id",
        "OwnerUserId",
        "CreationDate",
        "ParentId",
        "Score",
        "Body",
    ],
    "questions.csv": [
        "Row",
        "Id",
        "OwnerUserId",
        "CreationDate",
        "ClosedDate",
        "Score",
        "Title",
        "Body",
        "Tags",
    ],
}


class Reader:
    def __init__(self, splits, amount):
        self.splits = int(splits)
        self.amount = int(amount)

    def read(self, file):

        f = open(f"../archive/{file}")
        reader = csv.DictReader(f, fieldnames=row_names[file])
        length = 2014515 if self.amount < 0 else self.amount
        fist_col = next(reader)
        send = []
        total_splits = math.ceil(length / self.splits)
        for (i, row) in enumerate(reader):
            if i == self.amount:
                if len(send) > 0:
                    yield send
                break
            send.append(row)
            if i % self.splits == self.splits - 1:
                logger.info(
                    f"Reading {math.ceil(i / self.splits)} of {total_splits} total splits"
                )
                yield send
                send = []
        if len(send) > 0:
            yield send
