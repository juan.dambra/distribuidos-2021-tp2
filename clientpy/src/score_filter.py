#!/usr/bin/env python
from workers import Worker
from processors import Processor


def filter_func(msg):
    field = "Score"
    return True if int(msg[field]) > 10 else False


worker = Worker(Processor(None, filter_func))
worker.start()
worker.join()
