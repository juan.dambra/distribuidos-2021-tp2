#!/usr/bin/env python
from time import sleep
from workers import Worker
from processors import Processor


def map_func(msg, state):
    print(msg.keys())
    msg["Body"] = "New Body"
    # sleep(0.2)
    return msg


worker = Worker(Processor(map_func))
worker.start()
worker.join()
