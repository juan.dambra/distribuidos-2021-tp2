import logging
import os

# reduce log level
logging.getLogger("pika").setLevel(logging.WARNING)

LOGLEVEL = os.environ.get("LOGLEVEL", "INFO").upper()

if os.environ.get("DEBUG", "false").lower in ("true", "t", "1"):
    logging.basicConfig(
        format="%(name)s - [%(levelname)s] - %(message)s",
        level=logging.DEBUG,
        filename="debug.log",
    )
    logging.getLogger("").setLevel("DEBUG")
    c_handler = logging.StreamHandler()
    c_handler.setLevel(LOGLEVEL)
    c_format = logging.Formatter("%(name)s - [%(levelname)s] - %(message)s")
    c_handler.setFormatter(c_format)
    logging.getLogger("").addHandler(c_handler)
else:
    logging.basicConfig(
        format="%(name)s - [%(levelname)s] - %(message)s",
        level=LOGLEVEL,
    )
# logger = logging.Logger("RIC")
# logger = logging.getLogger(__name__)
# # Create handlers
