import pickle


WORKER_END = "end"
WORKER_START = "start"
WORKER_START_END = "worker_end"
WORKER_START_ACK = "worker_ack"
MESSAGE_TRIGGER = "trigger"
STATUS_ON = "ON"
STATUS_OFF = "OFF"

def decode_string(body):
    return pickle.loads(body)


def encode_to_string(body):
    return pickle.dumps(body)


class Message:
    def __init__(self, dict_arg=None):
        if not dict_arg is None:
            self.dict = dict_arg
            for key in dict_arg:
                setattr(self, key, dict_arg[key])

    def to_string(self):
        return pickle.dumps(self.dict)

    def from_string(msg):
        raw = pickle.loads(msg)
        return Message(dict_arg=raw)

    def from_dict(raw):
        return Message(dict_arg=raw)
