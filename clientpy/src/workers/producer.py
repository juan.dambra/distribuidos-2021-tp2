#!/usr/bin/env python
import pika
import os
import time
from src.utils.utils import Message, encode_to_string, MESSAGE_TRIGGER
from src.reader import Reader
from src.workers.worker import WorkerControl
from threading import Thread, Event
from src.utils.logger import logging

logger = logging.getLogger("Producer")


class Producer(Thread):
    def __init__(self, event_start, event_end, signal, input_file):
        Thread.__init__(self)
        self.event_start = event_start
        self.event_end = event_end
        self.signal = signal
        self.input_file = input_file
        self.timeout = int(os.environ.get("TIMEOUT", 10))
        # self.processor = processor
        step = os.environ.get("STEP")
        self.id = os.environ.get("ID")
        self.step = step
        self.next_step = os.environ.get("NEXT_STEP", int(step) + 1)
        self.id = os.environ.get("ID")
        self.service_name = os.environ.get("SERVICE_NAME")
        self.rabbitmq_host = os.environ.get("RABBITMQ_HOST", "rabbitmq")
        self.chunk_size = os.environ.get("CHUNK_SIZE", 1)
        self.punto = os.environ.get("PUNTO", "PUNTO1")
        self.total_length = os.environ.get("TOTAL_LENGTH", 2)

        logger.debug("INIT PRODUCER")

    def run(self):
        logger.debug("Producer waiting on start")

        logger.debug("Started Connection Producer Job")
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self.rabbitmq_host)
        )
        self.channel = connection.channel()
        self.channel.queue_declare(queue="control")
        self.channel.exchange_declare(exchange="jobs", exchange_type="topic")

        self.queue_next_step = f"{self.punto}.step.{self.next_step}.all"
        logger.debug(f"INIT:: queue to consume: {self.queue_next_step}")
        self.event_start.wait()

        reader = Reader(self.chunk_size, self.total_length)
        for chunk in reader.read(self.input_file):
            self.channel.basic_publish(
                exchange="jobs",
                routing_key=self.queue_next_step,
                body=encode_to_string(chunk),
            )
        message = Message.from_dict(
            {
                "message": "END",
                "id": self.id,
                "step": self.step,
                "next_step": self.next_step,
                "service_name": self.service_name,
                "type": MESSAGE_TRIGGER,
            }
        )
        self.channel.basic_publish(
            exchange="", routing_key="control", body=message.to_string()
        )
        logger.debug("INIT:: Sent End")
        logger.debug("INIT:: message published")
        self.event_end.wait()
        self.signal.set()


class WorkerProducer:
    def __init__(self, input_file) -> None:
        e = Event()
        e2 = Event()
        e3 = Event()
        self.t1 = WorkerControl(e, e2, e3)
        self.t2 = Producer(e, e2, e3, input_file)

    def start(self):
        self.t1.start()
        self.t2.start()

    def join(self):
        self.t1.join()
        self.t2.join()
        logger.debug("JOINT JOBS")
