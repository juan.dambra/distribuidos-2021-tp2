#!/usr/bin/env python
import pika
import os
import threading

from pika.spec import Channel
from src.processors.base_processor import BaseProcessor
from src.utils.utils import (
    STATUS_OFF,
    STATUS_ON,
    WORKER_END,
    WORKER_START,
    WORKER_START_END,
    WORKER_START_ACK,
    Message,
    encode_to_string,
)
from src.processors import Processor
from src.utils.logger import logging

logger = logging.getLogger("Worker")


class WorkerJob(threading.Thread):
    def __init__(self, event_start, event_end, signal, processor: BaseProcessor):
        threading.Thread.__init__(self)
        self.event_start = event_start
        self.event_end = event_end
        self.signal = signal
        self.processor = processor
        self.state = {"processor": processor.get_init_val()}
        self.timeout = int(os.environ.get("TIMEOUT", 10))
        self.rabbitmq_host = os.environ.get("RABBITMQ_HOST", "rabbitmq")
        self.id = os.environ.get("ID")
        step = os.environ.get("STEP")
        self.service_name = os.environ.get("SERVICE_NAME")
        self.step = step
        self.next_step = str(os.environ.get("NEXT_STEP", int(step) + 1))
        self.punto = os.environ.get("PUNTO", "PUNTO1")
        self.affinity = os.environ.get("AFFINITY", "False").lower() in (
            "true",
            "1",
            "t",
        )

    def setup(self):

        # queue_step = f"{self.punto}.step.{self.step}"
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self.rabbitmq_host, heartbeat=1800, blocked_connection_timeout=300
            )
        )
        self.channel = connection.channel()
        self.channel.exchange_declare(exchange="jobs", exchange_type="topic")
        if self.affinity:
            self.step_queue = (
                f"{self.punto}.step.{self.step}.{self.service_name}.{self.id}"
            )
            self.step_queue_topic = f"{self.punto}.step.{self.step}.*.{self.id}"
        else:
            self.step_queue = f"{self.punto}.step.{self.step}.{self.service_name}"
            self.step_queue_topic = f"{self.punto}.step.{self.step}.#"

        self.channel.queue_declare(queue=self.step_queue)
        self.channel.queue_bind(
            exchange="jobs",
            queue=self.step_queue,
            routing_key=self.step_queue_topic,
        )
        logger.debug(f"Binded queue on {self.step_queue}")
        self.queue_next_step = f"{self.punto}.step.{self.next_step}.all"
        self.event_start.wait()
        logger.debug("Starting Worker from Control")

    def process_message(self, message):
        method, properties, body = message
        logger.debug(f"Processing msg of len {len(body)}")
        self.processor.generate_callback(self.queue_next_step, self.state)(
            self.channel, method, properties, body
        )

    def run(self):
        self.setup()
        logger.debug("finished setup")
        # queue_step = f"{self.punto}.step.{self.step}"
        for message in self.channel.consume(
            self.step_queue, inactivity_timeout=self.timeout, auto_ack=True
        ):
            if message[2]:
                self.process_message(message)
            elif self.event_end.is_set():
                logger.debug("End Event is Recieved")
                if not self.processor.end_job is None:
                    res = self.processor.end_job(self.state)
                    logger.debug("is intermediate %s", self.processor.intermediate)
                    if not self.processor.intermediate and res:
                        logger.debug("result %s %s", res, self.queue_next_step)
                        self.channel.basic_publish(
                            exchange="jobs",
                            routing_key=self.queue_next_step,
                            body=encode_to_string(res),
                        )
                self.signal.set()
                break
            else:
                continue


class WorkerControl(threading.Thread):
    def __init__(self, event_start, event_end, signal):
        threading.Thread.__init__(self)
        self.event_start = event_start
        self.event_end = event_end
        self.signal = signal
        self.step = os.environ.get("STEP")
        self.next_step = os.environ.get("NEXT_STEP", int(self.step) + 1)
        self.rabbitmq_host = os.environ.get("RABBITMQ_HOST", "rabbitmq")
        self.punto = os.environ.get("PUNTO", "PUNTO1")
        self.id = os.environ.get("ID")
        self.service_name = os.environ.get("SERVICE_NAME")
        self.status = STATUS_OFF

    def start_message(self, channel: Channel):
        # message = Message(WORKER_START, "start", self.id, self.step)
        message = Message.from_dict(
            {
                "message": "start",
                "id": self.id,
                "step": self.step,
                "next_step": self.next_step,
                "service_name": self.service_name,
                "type": WORKER_START,
            }
        )
        channel.basic_publish(
            exchange="", routing_key="control", body=message.to_string()
        )

    def callback_control(self, channel, method_frame, header_frame, body):
        msg = Message.from_string(body)
        # logger.debug(f"Recieved Control MSG {msg}")
        if msg.type == WORKER_START_ACK:
            logger.debug("Confirmed Start, starting...")
            self.status = STATUS_ON
            self.event_start.set()
        if msg.type == WORKER_START_END:
            logger.debug("Recieved END, exiting...")
            message = Message.from_dict(
                {
                    "message": "end",
                    "id": self.id,
                    "step": self.step,
                    "next_step": self.next_step,
                    "service_name": self.service_name,
                    "type": WORKER_END,
                }
            )
            logger.debug("Event is Set")
            if self.status == STATUS_OFF:
                self.event_start.set()
            self.event_end.set()
            self.signal.wait()
            logger.debug("Job Is Done")
            channel.basic_publish(
                exchange="", routing_key="control", body=message.to_string()
            )
            channel.close()

    def setup(self):
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self.rabbitmq_host, heartbeat=1800, blocked_connection_timeout=300
            )
        )
        self.channel = connection.channel()
        self.channel.queue_declare(queue="control")

        self.channel.exchange_declare(
            exchange="control-coordinator", exchange_type="topic"
        )
        self.channel.exchange_declare(
            exchange="control-coordinator-global", exchange_type="fanout"
        )
        step_queue = f"step.{self.step}"
        result = self.channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue
        self.channel.queue_bind(
            exchange="control-coordinator",
            queue=queue_name,
            routing_key=step_queue,
        )
        logger.debug(f"Binded queue on {step_queue}")
        self.channel.basic_consume(
            queue=queue_name, on_message_callback=self.callback_control, auto_ack=True
        )
        result = self.channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue
        self.channel.queue_bind(
            exchange="control-coordinator-global",
            queue=queue_name,
        )
        logger.debug(f"Binded FANOUT queue on {queue_name}")
        self.channel.basic_consume(
            queue=queue_name, on_message_callback=self.callback_control, auto_ack=True
        )

    def run(self):
        self.setup()
        self.start_message(self.channel)
        logger.debug(f"STARTED WORKER {self.id} on step {self.step} ")
        self.channel.start_consuming()


class Worker:
    def __init__(self, processor) -> None:
        e = threading.Event()
        e2 = threading.Event()
        e3 = threading.Event()
        self.t1 = WorkerControl(e, e2, e3)
        self.t2 = WorkerJob(e, e2, e3, processor)

    def start(self):
        self.t1.start()
        self.t2.start()

    def join(self):
        self.t1.join()
        self.t2.join()
        logger.debug("JOINT JOBS")
