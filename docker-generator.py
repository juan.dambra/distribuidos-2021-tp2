import os
from pickle import FALSE
import yaml
import textwrap


def read_yaml(file_path):
    with open(file_path, "r") as f:
        return yaml.safe_load(f)


config = read_yaml("./tp2-config.yml")
rabbit_host = config["RABBIT_HOST"]
debug = config["DEBUG"]
loglevel = config["LOGLEVEL"]

worker_common_env = f"""\
    - PYTHONUNBUFFERED=1
    - RABBIT_HOST={rabbit_host}
    - DEBUG={debug}
"""
worker_common_volumes = """\
    volumes:
      - ./clientpy:/app
      - ~/nltk_data:/root/nltk_data
      - ./archive:/archive\
"""
worker_common_depends = """\
    depends_on:
      - rabbitmq
"""

header = """\
version: "3.9"
services:
"""
rabbit = """\
  rabbitmq:
    image: rabbitmq:3-management-alpine
    ports:
      - 5672:5672
      - 15672:15672
    logging:
      driver: none
"""


def generate_worker(id, step, script, name="clientpy", custom_env=None):
    d = {
        "environment": [
            "PYTHONUNBUFFERED=1",
            f"LOGLEVEL={loglevel}",
            "PYTHONPATH=/app",
            f"RABBITMQ_HOST={rabbit_host}",
            f"ID={id}",
        ]
    }
    if not custom_env is None:
        d["environment"] += custom_env
    str_env = textwrap.indent(yaml.dump(d), 4 * " ")
    container_name = f"{name}-{step}-{id}"
    client = f"""
  {container_name}:
    build: clientpy
    command: ["python", "src/{script}.py"]
{str_env}
{worker_common_volumes}
{worker_common_depends}
    """
    return client


coordinator = """\
  coordinator:
    build: clientpy
    command: ["python", "src/coordinator.py"]
    environment:
      - ID=0
      - PYTHONUNBUFFERED=1
    volumes:
      - ./clientpy:/app
    depends_on:
      - rabbitmq
"""


def generate_env(tier_1, tier_2=None):
    envs = []
    items = config[tier_1].items()
    for key, val in items:
        if type(val) != dict and type(val) != list:
            envs.append(f"{key}={val}")
    if not tier_2 is None:
        items = tier_2.items()
        for key, val in items:
            if type(val) != dict and type(val) != list:
                envs.append(f"{key}={val}")
    return envs


def generate_punto(punto_str):
    for etapa in config[punto_str]["ETAPAS"]:
        for id in range(etapa["WORKERS"]):
            service_name = dict.get(etapa, "SERVICE_NAME", None)
            runner = etapa["RUNNER"]
            step = etapa["STEP"]
            f.write(
                generate_worker(
                    f"{id}",
                    step,
                    runner,
                    service_name,
                    custom_env=generate_env(punto_str, etapa),
                )
            )


f = open("docker-compose.yml", "w")
f.write(header)
f.write(rabbit)


def generate_coordinator(punto_str):
    punto1_etapas = len(config[punto_str]["ETAPAS"])
    coordinator_env = generate_env(punto_str)
    coordinator_env += [f"AMOUNT_STEPS={punto1_etapas}"]
    f.write(
        generate_worker(
            "coordinator",
            "",
            "coordinator",
            "coordinator",
            custom_env=coordinator_env,
        )
    )


punto_env = os.environ.get("PUNTO", False)
if punto_env:
    punto = punto_env
else:
    punto = input("Ingrese Punto: 1, 2 o 3: ")
if punto == "1":
    punto_str = "PUNTO1"
elif punto == "2":
    punto_str = "PUNTO2"
elif punto == "3":
    punto_str = "PUNTO3"
elif punto == "0":
    punto_str = "PUNTO0"
else:
    exit(-1)

generate_coordinator(punto_str)
generate_punto(punto_str)
print(f"Generated {punto_str}")

